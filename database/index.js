require('dotenv').config();

const connection = {
    mongo:() => {
        const mongoose = require('mongoose');
        let DB_URI = process.env.DB_HOST;
        DB_URI = DB_URI.replace(/<usuario>/,process.env.DB_USER);
        DB_URI = DB_URI.replace(/<password>/,process.env.DB_PASSWORD);
        mongoose.set('strictQuery', false);
        mongoose.connect(DB_URI,{
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        .then(()=>{
            if(process.env.APP_ENV !== 'production')
                console.log('Database connected');            
        }).catch((err)=>{
            console.log("Something went wrong");
            console.log(err);
        });
    }
}

module.exports = connection;