const express = require('express');
const router = express.Router();
const fs = require('fs');

const PATH_ROUTES = __dirname;
const extractName = (file) => file.replace('.js', '');
fs.readdirSync(PATH_ROUTES).forEach(file => {
    if(file === 'index.js') return;    
    const name = extractName(file);
    try{
        router.use(`/${name}`, require(`./${file}`));  
    }catch(e){
        console.log("error");
    }
});//*/


module.exports = router;