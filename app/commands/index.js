require('dotenv').config();
const fs = require('fs');
const path = require('path');
const commands =  require('./commands');
const templates = require('./templates');
const handleUtils = require('../utils/handleUtils');

//process.argv.splice(2),

let commandlist = process.argv.splice(2);
commandlist.forEach((arg, index) => {
    if(arg.startsWith('make:')){
        commands.create = arg.split(':')[1];
    }else{
        commands.name = arg
    }    
});

let create = {
    controller:(name) => {
        
        if(!fs.existsSync(templates.DIR_PATH.controller)){
            fs.mkdirSync(templates.DIR_PATH.controller,{recursive:true});
        }

        let template = templates.templates.controller.replace(/{{ModelName}}/g, handleUtils.toCapital(name));
        template = template.replace(/{{Model}}/g, name);        
        const PATH_FILE = path.join(templates.DIR_PATH.controller,`${name}.js`);

        fs.writeFileSync(PATH_FILE,template);
        if(!fs.existsSync(PATH_FILE)){
            console.log(`can't create controller`);
        }

        console.log(`controller ${name} created!`);
    },
    model:(name) => {
        const typeModel = process.env.DB_ENGINE;
        let template = templates.templates.model[typeModel].replace(/{{ModelName}}/g, handleUtils.toCapital(name));
        

        if(!fs.existsSync(path.join(templates.DIR_PATH.model,typeModel))){
            fs.mkdirSync(path.join(templates.DIR_PATH.model,typeModel),{recursive:true});
        }

        const PATH_FILE = path.join(templates.DIR_PATH.model,typeModel,`${name}.js`);

        fs.writeFileSync(PATH_FILE,template);
        if(!fs.existsSync(PATH_FILE)){
            console.log(`can't create model`);
        }

        console.log(`model ${name} created!`);
    },
    route:(name) => {
        let template = templates.templates.route.replace(/{{controller}}/g, name.toLowerCase());
        if(!fs.existsSync(templates.DIR_PATH.router)){
            fs.mkdirSync(templates.DIR_PATH.router,{recursive:true});
        }

        const PATH_FILE = path.join(templates.DIR_PATH.router,`${name}.js`);
        fs.writeFileSync(PATH_FILE,template);
        if(!fs.existsSync(templates.DIR_PATH.router,`${name}.js`)){
            console.log(`can't create route`);
        }

        console.log(`route ${name} created!`);

    }
}

try{

    if(!create[commands.create]){
        console.log('please check the command list');
        console.table(Object.keys(create).map(key => `make:${key}`));
    }
    create[commands.create](commands.name);
}catch(e){
    console.error(`${e}`);    
}
