const path = require('path');
const MAIN_PATH = path.resolve(__dirname, '../../');

const DIR_PATH = {
    controller: path.join(MAIN_PATH, 'app', 'http', 'controllers'),
    model: path.join(MAIN_PATH, 'app', 'models'),
    router: path.join(MAIN_PATH, 'routes'),
}

const templates = {
    controller : `
    const path = require('path');
    const {matchedData} = require('express-validator');
    const { {{Model}} } = require('../../models');

    //add methods to manage the controller

    module.exports = {

    }
    `,
    model:{
        nosql:`
        const mongoose = require('mongoose');

        const {{ModelName}}Schema = new mongoose.Schema(
            {

            },
            {
                timestamps: true,
                versionKey: false
            }
        );

        const {{ModelName}} = mongoose.model('{{ModelName}}', {{ModelName}}Schema);

        module.exports = {{ModelName}}
        
        `,
        sql:``,
    },
    route:`
    const express = require('express');
    const router = express.Router();
    const path = require('path');
    const APP_PATH = path.join(__dirname, '../app');
    const {{controller}} = require(`+'`${APP_PATH}/http/controllers/{{controller}}.js`'+`);

    //define routes


    module.exports = router
    `
}

module.exports = {
    MAIN_PATH, 
    DIR_PATH,
    templates,
}