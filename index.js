require('dotenv').config();
const connection = require('./database');

function iniciarServidor(puerto){
    const express = require('express');
    const app = express();
    const cors = require('cors');
    app.use(cors());
    app.use(express.json());
    // app.use(express.urlencoded({extended:true}));

    //TODO: DEFINE ROUTE STAORAGE
    app.use(express.static('storge/app/public'));
    app.use('/api', require('./routes'));
    

    app.listen(puerto,()=>{
        console.log(`Proyecto Condominio escuchando en el puerto ${puerto}`);
    });

    connection[process.env.DB_CONNECTION]();
}
    

iniciarServidor(process.env.APP_PORT);


